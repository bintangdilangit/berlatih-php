    <?php
    function ubah_huruf($string)
    {
        $chars = preg_split('//', $string, -1, PREG_SPLIT_NO_EMPTY);
        $strFix = "";
        for ($i = 0; $i < strlen($string); $i++) {
            $strFix[$i] = ++$chars[$i];
        }
        echo "<br>" . $strFix;
    }

    // TEST CASES
    echo ubah_huruf('wow'); // xpx
    echo ubah_huruf('developer'); // efwfmpqfs
    echo ubah_huruf('laravel'); // mbsbwfm
    echo ubah_huruf('keren'); // lfsfo
    echo ubah_huruf('semangat'); // tfnbohbu

    ?>